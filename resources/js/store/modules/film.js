import axios from 'axios'
import * as types from '../mutation-types'
// state
export const state = {
    film: [],
    next: [],
    previous: []
}

// getters
export const getters = {
    film: state => state.film,
    previous: state => state.previous,
    next: state => state.next

}

// mutations
export const mutations = {
    [types.FETCH_FILMS_SUCCESS] (state, { film }) {
        state.film = film.film
        state.previous = film.previous
        state.next = film.next
    }
}

export const actions = {

    async fetchFilm ({ commit }, id) {
        try {
          if(id && id.id != null){
            
            id = id.id
            const { data } = await axios.get('/api/films/' + id)
            commit(types.FETCH_FILMS_SUCCESS, { film: data })
          }
          else{
            const { data } = await axios.get('/api/films')
            commit(types.FETCH_FILMS_SUCCESS, { film: data })
          }

          
        } catch (e) {
          commit(types.FETCH_FILMS_ERROR)
        }
      }
}
