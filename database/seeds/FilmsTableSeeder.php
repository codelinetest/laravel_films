<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data1 = DB::table('films')->insertGetId([
            'name' => 'Avengers - End Game',
            'slug' => 'avengers-end-game',
            'description' => 'After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to undo Thanos actions and restore order to the universe.',
            'release_date' => '2019-04-26',
            'rating' => 5,
            'ticket_price' => 22,
            'country' => 'USA',
            'photo' => 'avengers.jpg'
        ]);
        $data2 = DB::table('films')->insertGetId([
            'name' => 'Dark Phoenix',
            'slug' => 'dark-phoenix',
            'description' => 'Jean Grey begins to develop incredible powers that corrupt and turn her into a Dark Phoenix. Now the X-Men will have to decide if the life of a team member is worth more than all the people living in the world.',
            'release_date' => '2019-07-07',
            'rating' => 5,
            'ticket_price' => 20,
            'country' => 'USA',
            'photo' => 'DarkPhoenix.jpg'
        ]);
        $data3 = DB::table('films')->insertGetId([
            'name' => 'Men in Black: International',
            'slug' => 'men-in-black-international',
            'description' => 'The Men in Black have always protected the Earth from the scum of the universe. In this new adventure, they tackle their biggest threat to date: a mole in the Men in Black organization.',
            'release_date' => '2019-07-14',
            'rating' => 4,
            'ticket_price' => 18,
            'country' => 'USA',
            'photo' => 'men-in-black-international-poster.jpg'
        ]);
        $data4 = DB::table('films')->insertGetId([
            'name' => 'Spider-Man: Far from Home',
            'slug' => 'spider-man-far-from-home',
            'description' => 'Peter Parker and his friends go on a European vacation, where Peter finds himself agreeing to help Nick Fury uncover the mystery of several elemental creature attacks, creating havoc across the continent.',
            'release_date' => '2019-07-05',
            'rating' => 5,
            'ticket_price' => 22,
            'country' => 'USA',
            'photo' => 'Spider-Man-Far-from-Home.jpg'
        ]);
        $data5 = DB::table('films')->insertGetId([
            'name' => 'John Wick: Chapter 3 - Parabellum',
            'slug' => 'john-wick-chapter-3-parabellum',
            'description' => 'Super-assassin John Wick is on the run after killing a member of the international assassins guild, and with a $14 million price tag on his head - he is the target of hit men and women everywhere.',
            'release_date' => '2019-05-17',
            'rating' => 5,
            'ticket_price' => 20,
            'country' => 'USA',
            'photo' => 'John-Wick-Chapter-3-poster-2.jpg'
        ]);

        $tags1 = DB::table('taggable_tags')->insertGetId([
            'name' => 'Action',
            'normalized' => 'action'
        ]);
        $tags2 = DB::table('taggable_tags')->insertGetId([
            'name' => 'Adventure',
            'normalized' => 'adventure'
        ]);
        $tags3 = DB::table('taggable_tags')->insertGetId([
            'name' => 'Fantasy',
            'normalized' => 'fantasy'
        ]);
        $tags4 = DB::table('taggable_tags')->insertGetId([
            'name' => 'Crime',
            'normalized' => 'crime'
        ]);

        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data1,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags1
        ]);
        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data1,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags2
        ]);
        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data1,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags3
        ]);

        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data2,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags1
        ]);
        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data2,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags2
        ]);
        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data2,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags3
        ]);

        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data3,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags1
        ]);
        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data3,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags2
        ]);
        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data3,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags3
        ]);

        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data4,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags1
        ]);
        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data4,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags2
        ]);
        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data4,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags3
        ]);

        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data5,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags1
        ]);
        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data5,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags2
        ]);
        DB::table('taggable_taggables')->insertGetId([
            'taggable_id' => $data5,
            'taggable_type' => 'App\Films',
            'tag_id' => $tags4
        ]);



    }
}
