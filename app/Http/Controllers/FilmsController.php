<?php

namespace App\Http\Controllers;

use App\Films;
use Illuminate\Http\Request;
use Image;
class FilmsController extends Controller
{
    /**
    * Display a listing of the resource.
    * 
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        
        $film = Films::orderBy('id', 'asc')->first();
        $film->genre = $film->tagList;

        // get previous film id
        $previous = Films::where('id', '<', $film->id)->select('id', 'name', 'slug', 'photo')->orderBy('id','desc')->first();

        // get next film id
        $next = Films::where('id', '>', $film->id)->select('id', 'name', 'slug', 'photo')->orderBy('id','asc')->first();

        return [
            'film' => $film,
            'next' => $next,
            'previous' => $previous
        ];

    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(array $data)
    {
        
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required',
            'release_date' => 'required|date',
            'rating' => 'required|integer',
            'ticket_price' => 'required',
            'country' => 'required',
            'photo' => 'required',
            'genre' => 'required'
        ]);

        // create film
        $film = Films::create([
            'name' => $request->input('name'), 
            'description' => $request->input('description'), 
            'release_date' => $request->input('release_date'),
            'rating' => $request->input('rating'),
            'ticket_price' => $request->input('ticket_price'),
            'country' => $request->input('country')
            ]);

            // upload photo
            $photo_url = "poster-" . str_random(10) . ".jpg";
            $photo_path = public_path() .'/images/' . $photo_url;
            $photo = explode(',', $request->input('photo'));
            Image::make(base64_decode($photo[1]))->save($photo_path);

            // set photo to new film
            $film->photo = $photo_url;
            $film->save();

            // add genre
            $film->tag($request->input('genre'));

            return $film;

    }
        
        /**
        * Display the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function show($slug)
        {
            $film = Films::where('slug', $slug)->first();
            $film->genre = $film->tagList;

            // get previous film id
            $previous = Films::where('id', '<', $film->id)->select('id', 'name', 'slug', 'photo')->orderBy('id','desc')->first();

            // get next film id
            $next = Films::where('id', '>', $film->id)->select('id', 'name', 'slug', 'photo')->orderBy('id','asc')->first();

            return [
                'film' => $film,
                'next' => $next,
                'previous' => $previous
            ];
        }
        
        /**
        * Show the form for editing the specified resource.
        *
        * @param  \App\Films  $films
        * @return \Illuminate\Http\Response
        */
        public function edit(Films $films)
        {
            //
        }
        
        /**
        * Update the specified resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function update(Request $request, $id)
        {
            $film = Films::find($id);
            $film->update($request->all());
            return $film;
        }
        
        /**
        * Remove the specified resource from storage.
        *
        * @param  \App\Films  $films
        * @return \Illuminate\Http\Response
        */
        public function destroy(Films $films)
        {
            //
        }
    }
