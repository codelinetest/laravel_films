# Laravel Film Base

## Build on top of

- Laravel 5.8 (APIs)
- Vue + VueRouter + Vuex + VueI18n + ESlint
- Login, register and password reset
- Authentication with JWT
- Bootstrap 4 + Font Awesome 5 + SASS

## Installation

- Edit `.env` and set your database connection details
- `php artisan migrate`
- `npm install`

#### Development

```bash
# build and watch
npm run watch

# Local Server (localhost:8000)
php artisan serve
```
#### Database

```
# Tables
php artisan migrate

# Sample Data Seeds
php artisan db:seed --class=FilmsTableSeeder
```

### Screenshots
## Home Page
![image](https://s3-ap-southeast-1.amazonaws.com/codeline-demo/screenshot-1.png)
![image](https://s3-ap-southeast-1.amazonaws.com/codeline-demo/screenshot-2.png)

## Login
![image](https://s3-ap-southeast-1.amazonaws.com/codeline-demo/screenshot-auth.png)

## Register
![image](https://s3-ap-southeast-1.amazonaws.com/codeline-demo/screenshot-register.png)

## Add Film
![image](https://s3-ap-southeast-1.amazonaws.com/codeline-demo/screenshot-add-film.png)
![image](https://s3-ap-southeast-1.amazonaws.com/codeline-demo/screenshot-add-film-2.png)